import { Typography, Box } from '@mui/material'
import BikeType from 'components/BikeType'

type BikeBookedType = {
  bikeImage: string
  bikeName: string
  bikeType: string
}

const BikeBooked = ({ bikeImage, bikeName, bikeType }: BikeBookedType): JSX.Element => {
  return (
    <Box display='flex' flexDirection='column' alignItems='center'>
      <Typography fontWeight='bold' mb={3} mt={5} fontSize={20}>
        Thank you!
      </Typography>
      <Typography mb={3}>Your bike is booked.</Typography>
      <img src={bikeImage} alt='rented bike' width='300px' height='100%' />
      <Typography fontWeight='bold' mt={2} mb={1}>
        {bikeName}
      </Typography>
      <BikeType type={bikeType} />
    </Box>
  )
}

export default BikeBooked
