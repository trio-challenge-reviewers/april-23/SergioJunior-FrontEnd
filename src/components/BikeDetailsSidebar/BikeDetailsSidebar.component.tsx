import { BookingButton, InfoIcon, OverviewContainer, PriceRow } from './BikeDetails.styles'
import DatePicker, { DatePickerType } from 'components/DatePicker'
import BikeBooked from 'components/BikeBooked'
import { Box, Divider, Typography } from '@mui/material'
import Bike from 'models/Bike'

export type BikeDetailsProps = DatePickerType & {
  bike?: Bike
  total: number
  isBikeBooked: boolean
  onSubmitBikeRent: () => void
  errorMessage: string
  isLoading: boolean
  isSubmitButtonDisabled: boolean
  rateByDay: number
  servicesFee: number
}

const BikeDetailsSidebarComponent = ({
  isBikeBooked,
  onSubmitBikeRent,
  isSubmitButtonDisabled,
  rateByDay,
  total,
  errorMessage,
  bike,
  setDateRange,
  dateRange,
  servicesFee,
  isLoading,
}: BikeDetailsProps): JSX.Element => {
  return (
    <OverviewContainer variant='outlined' data-testid='bike-overview-container'>
      {isBikeBooked ? (
        <BikeBooked
          bikeType={bike?.type ?? ''}
          bikeName={bike?.name ?? ''}
          bikeImage={bike?.imageUrls[0] ?? ''}
        />
      ) : (
        <>
          <DatePicker setDateRange={setDateRange} dateRange={dateRange} />
          <Box pl={3} pr={3}>
            <Typography variant='h2' fontSize={16} marginBottom={1.25}>
              Booking Overview
            </Typography>

            <Divider />

            <PriceRow marginTop={1.75} data-testid='bike-overview-single-price'>
              <Box display='flex' alignItems='center'>
                <Typography marginRight={1}>Subtotal</Typography>
                <InfoIcon fontSize='small' />
              </Box>

              <Typography>{rateByDay} €</Typography>
            </PriceRow>

            <PriceRow marginTop={1.5} data-testid='bike-overview-single-price'>
              <Box display='flex' alignItems='center'>
                <Typography marginRight={1}>Service Fee</Typography>
                <InfoIcon fontSize='small' />
              </Box>

              <Typography>{servicesFee} €</Typography>
            </PriceRow>
            {isLoading && (
              <Box position='relative'>
                <Box
                  width={40}
                  mr='auto'
                  ml='auto'
                  left={0}
                  right={0}
                  top={-55}
                  position='absolute'
                  className='loading-animation'
                />
              </Box>
            )}

            {!!total && (
              <PriceRow marginTop={1.75} data-testid='bike-overview-total'>
                <Typography fontWeight={800} fontSize={16}>
                  Total
                </Typography>
                <Typography variant='h2' fontSize={24} letterSpacing={1}>
                  {total} €
                </Typography>
              </PriceRow>
            )}

            {errorMessage && (
              <Typography
                pl={2}
                pt={1}
                pb={1}
                mt={2}
                bgcolor='#d32f2f'
                borderRadius={0.2}
                color='white'
              >
                {errorMessage}
              </Typography>
            )}
            <BookingButton
              fullWidth
              disabled={isSubmitButtonDisabled}
              onClick={onSubmitBikeRent}
              disableElevation
              variant='contained'
              data-testid='bike-booking-button'
            >
              Add to booking
            </BookingButton>
          </Box>
        </>
      )}
    </OverviewContainer>
  )
}

export default BikeDetailsSidebarComponent
