import BikeDetailsSidebar, { BikeDetailsProps } from './BikeDetailsSidebar.component'

export type BikeDetailsType = BikeDetailsProps

export default BikeDetailsSidebar
