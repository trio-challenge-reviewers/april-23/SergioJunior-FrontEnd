import { DatePicker as MantineDatePicker } from '@mantine/dates'
import { DatePickerBox } from './DatePicker.styles'
import { Typography } from '@mui/material'
import { addDays } from 'date-fns'
import './DatePickerExtendedCSS.css'
import { DatePickerProps } from './DatePicker.container'

export type DatePickerComponentProps = DatePickerProps & {
  updateDatePickerHeaderDOM: () => void
}

const DatePicker = ({
  dateRange,
  setDateRange,
  updateDatePickerHeaderDOM,
}: DatePickerComponentProps): JSX.Element => {
  return (
    <DatePickerBox>
      <Typography pl={3} fontWeight='bold' mb={0.5} fontSize={22}>
        Select date and time
      </Typography>
      <MantineDatePicker
        firstDayOfWeek={0}
        onNextMonth={updateDatePickerHeaderDOM}
        onPreviousMonth={updateDatePickerHeaderDOM}
        monthLabelFormat='MMMM/YYYY'
        minDate={addDays(new Date(), 1)}
        type='range'
        value={dateRange}
        onChange={(event) => {
          setDateRange(event)
          updateDatePickerHeaderDOM()
        }}
      />
    </DatePickerBox>
  )
}

export default DatePicker
