import './DatePickerExtendedCSS.css'
import DatePickerComponent from './DatePicker.component'
import { useCallback, useEffect } from 'react'

export type DatePickerProps = {
  dateRange: [Date | null, Date | null]
  setDateRange: (value: [Date | null, Date | null]) => void
}

const DatePicker = ({ dateRange, setDateRange }: DatePickerProps): JSX.Element => {
  const updateDatePickerHeaderDOM = useCallback(() => {
    setTimeout(() => {
      const newElementClass = '.fake-date-period'
      const buttoWithPeriodClass = '.mantine-CalendarHeader-calendarHeaderLevel'
      const datePickerHeaderClass = '.mantine-CalendarHeader-calendarHeader'

      document.querySelector(newElementClass)?.remove()
      const datePickerHeader = document.querySelector(datePickerHeaderClass)

      const buttonWithPeriod = document.querySelector(buttoWithPeriodClass)
      const selectedPeriodText = buttonWithPeriod?.textContent ?? ''
      const [month, year] = selectedPeriodText.split('/')

      if (datePickerHeader) {
        datePickerHeader.insertAdjacentHTML(
          'afterbegin',
          `<div class="${newElementClass.replace('.', '')}">
            <p class="period-month">${month}</p>
            <p class="period-year">${year}</p>
          </div>`,
        )
      }
    }, 0)
  }, [])

  useEffect(() => {
    updateDatePickerHeaderDOM()
  }, [])

  return (
    <DatePickerComponent
      updateDatePickerHeaderDOM={updateDatePickerHeaderDOM}
      dateRange={dateRange}
      setDateRange={setDateRange}
    />
  )
}

export default DatePicker
