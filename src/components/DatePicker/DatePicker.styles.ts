import { Box, BoxProps, styled } from '@mui/material'

export const DatePickerBox = styled(Box)<BoxProps>(() => ({
  position: 'relative',
  marginBottom: '20px',
}))
