import DatePicker, { DatePickerProps } from './DatePicker.container'

export type DatePickerType = DatePickerProps

export default DatePicker
