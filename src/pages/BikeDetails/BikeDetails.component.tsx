import { Box, Breadcrumbs, Divider, Link, Typography } from '@mui/material'
import BikeImageSelector from 'components/BikeImageSelector'
import BikeSpecs from 'components/BikeSpecs'
import BikeType from 'components/BikeType'
import BookingAddressMap from 'components/BookingAddressMap'
import Header from 'components/Header'
import {
  BreadcrumbContainer,
  BreadcrumbHome,
  BreadcrumbSeparator,
  Content,
  DetailsContainer,
  FavoriteIcon,
  LikeButton,
  PriceRow,
} from './BikeDetails.styles'
import BikeDetailsSidebar, { BikeDetailsType } from 'components/BikeDetailsSidebar'

type BikeDetailsProps = BikeDetailsType & {
  rateByWeek: number
}

const BikeDetails = ({
  isSubmitButtonDisabled,
  bike,
  total,
  isBikeBooked,
  onSubmitBikeRent,
  setDateRange,
  dateRange,
  errorMessage,
  isLoading,
  rateByDay,
  servicesFee,
  rateByWeek,
}: BikeDetailsProps) => {
  return (
    <div data-testid='bike-details-page'>
      <Header />

      <BreadcrumbContainer data-testid='bike-details-breadcrumbs'>
        <Breadcrumbs separator={<BreadcrumbSeparator />}>
          <Link underline='hover' display='flex' alignItems='center' color='white' href='/'>
            <BreadcrumbHome />
          </Link>

          <Typography fontWeight={800} letterSpacing={1} color='white'>
            {bike?.name}
          </Typography>
        </Breadcrumbs>
      </BreadcrumbContainer>

      <Content>
        <DetailsContainer variant='outlined' data-testid='bike-details-container'>
          {!!bike?.imageUrls && <BikeImageSelector imageUrls={bike.imageUrls} />}

          <BikeSpecs bodySize={bike?.bodySize} maxLoad={bike?.maxLoad} ratings={bike?.ratings} />

          <Divider />

          <Box marginY={2.25}>
            <Box display='flex' alignItems='center' justifyContent='space-between'>
              <div>
                <Typography
                  variant='h1'
                  fontSize={38}
                  fontWeight={800}
                  marginBottom={0.5}
                  data-testid='bike-name-details'
                >
                  {bike?.name}
                </Typography>

                <BikeType type={bike?.type} />
              </div>

              <LikeButton>
                <FavoriteIcon />
              </LikeButton>
            </Box>

            <Typography marginTop={1.5} fontSize={14}>
              {bike?.description}
            </Typography>
          </Box>

          <Divider />

          <Box marginY={2.25} data-testid='bike-prices-details'>
            <PriceRow>
              <Typography>Day</Typography>
              <Typography fontWeight={800} fontSize={24} letterSpacing={1}>
                {rateByDay} €
              </Typography>
            </PriceRow>

            <PriceRow>
              <Typography>Week</Typography>
              <Typography fontWeight={800} fontSize={24} letterSpacing={1}>
                {rateByWeek} €
              </Typography>
            </PriceRow>
          </Box>

          <Divider />

          <Box marginTop={3.25}>
            <Typography variant='h1' fontSize={24} fontWeight={800}>
              Full adress after booking
            </Typography>

            <BookingAddressMap />
          </Box>
        </DetailsContainer>
        <BikeDetailsSidebar
          isBikeBooked={isBikeBooked}
          onSubmitBikeRent={onSubmitBikeRent}
          isSubmitButtonDisabled={isSubmitButtonDisabled}
          rateByDay={rateByDay}
          total={total}
          errorMessage={errorMessage}
          bike={bike}
          setDateRange={setDateRange}
          dateRange={dateRange}
          servicesFee={servicesFee}
          isLoading={isLoading}
        />
      </Content>
      <Box
        height='100vh'
        width='100vw'
        left={0}
        position='fixed'
        top={0}
        className='lottie-animation'
      />
    </div>
  )
}

export default BikeDetails
