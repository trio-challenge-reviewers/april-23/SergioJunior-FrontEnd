import Bike from 'models/Bike'
import lottie from 'lottie-web'
import { useEffect, useState, useCallback } from 'react'
import { useLocation } from 'react-router-dom'
import BikeDetails from './BikeDetails.component'
import * as BikeServices from 'services/bike'
import { getServicesFee, formatRangeDates } from './BikeDetails.utils'
import { CANDIDATE_ID } from 'config'

type StateReceived = {
  bike: Bike
}

const defaultBikeRentSubmit = {
  value: 0,
  isLoading: false,
  errorMessage: '',
  isBooked: false,
}

const defaultBikeRentAmount = {
  value: 0,
  isLoading: false,
  errorMessage: '',
}

const BikeDetailsContainer = () => {
  const { state } = useLocation()
  const [dateRange, setDateRange] = useState<[Date | null, Date | null]>([null, null])
  const [currentBikeData, setCurrentBikeData] = useState<Bike>()
  const [bikeRentAmount, setBikeRentAmount] = useState(defaultBikeRentAmount)
  const [bikeRentSubmit, setBikeRentSubmit] = useState(defaultBikeRentSubmit)

  useEffect(() => {
    window.scroll({
      top: 0,
    })
    if (state) {
      const { bike } = state as StateReceived
      setCurrentBikeData(bike)
    }
  }, [])

  const onSubmitBikeRent = useCallback(async () => {
    try {
      const [dateFrom, dateTo] = formatRangeDates(dateRange as [Date, Date])
      setBikeRentSubmit((prev) => ({
        ...prev,
        errorMessage: '',
        isLoading: true,
      }))
      await BikeServices.submitBikeRent({
        bikeId: (currentBikeData?.id ?? 0) as number,
        dateFrom,
        dateTo,
        userId: CANDIDATE_ID,
      })
      setBikeRentSubmit((prev) => ({
        ...prev,
        isBooked: true,
        isLoading: false,
      }))
    } catch (error: any) {
      setBikeRentSubmit((prev) => ({
        ...prev,
        isLoading: false,
        errorMessage: error?.response?.data?.message,
      }))
    }
  }, [dateRange, currentBikeData])

  const fetchBikeRentAmount = async () => {
    try {
      const [dateFrom, dateTo] = formatRangeDates(dateRange as [Date, Date])
      setBikeRentAmount((prev) => ({
        ...prev,
        isLoading: true,
        errorMessage: '',
      }))
      const showAmountResponse = await BikeServices.getBikeRentAmount({
        bikeId: (currentBikeData?.id ?? 0) as number,
        dateFrom,
        dateTo,
        userId: CANDIDATE_ID,
      })
      setBikeRentAmount((prev) => ({
        ...prev,
        isLoading: false,
        value: showAmountResponse.data.totalAmount,
      }))
    } catch (error: any) {
      setBikeRentAmount((prev) => ({
        ...prev,
        isLoading: false,
        errorMessage: error?.response?.data?.message,
        value: 0,
      }))
    }
  }

  useEffect(() => {
    if ((currentBikeData?.id, dateRange[0], dateRange[1])) {
      fetchBikeRentAmount()
    }
  }, [dateRange, currentBikeData])

  useEffect(() => {
    if (!dateRange[0] || !dateRange[1]) {
      setBikeRentAmount(defaultBikeRentAmount)
    }
  }, [dateRange])

  useEffect(() => {
    const confettiAnimationElement = document.querySelector('.lottie-animation')
    const loadingAnimationElement = document.querySelector('.loading-animation')
    const isLoadingAnimation = bikeRentAmount.isLoading
    const container = isLoadingAnimation ? loadingAnimationElement : confettiAnimationElement
    const confettiAnimation = 'https://assets6.lottiefiles.com/packages/lf20_rovf9gzu.json'
    const loadingAnimation = 'https://assets10.lottiefiles.com/packages/lf20_a2chheio.json'
    const path = isLoadingAnimation ? loadingAnimation : confettiAnimation
    const hasFoundElements = loadingAnimationElement || confettiAnimationElement

    if (hasFoundElements && (bikeRentSubmit.isBooked || bikeRentAmount.isLoading)) {
      lottie.loadAnimation({
        container: container as Element,
        renderer: 'svg',
        loop: isLoadingAnimation,
        autoplay: true,
        path,
      })
    }
  }, [bikeRentSubmit.isBooked, bikeRentAmount.isLoading])

  const errorMessage = bikeRentAmount.errorMessage || bikeRentSubmit.errorMessage
  const isLoading = bikeRentAmount.isLoading || bikeRentSubmit.isLoading
  const isSubmitButtonDisabled = !dateRange[0] || !dateRange[1] || isLoading || !!errorMessage
  const rateByDay = currentBikeData?.rate || 0
  const rateByWeek = rateByDay * 7
  const servicesFee = getServicesFee(rateByDay)

  return (
    <BikeDetails
      rateByDay={rateByDay}
      rateByWeek={rateByWeek}
      servicesFee={servicesFee}
      isLoading={isLoading}
      errorMessage={errorMessage}
      isBikeBooked={bikeRentSubmit.isBooked}
      isSubmitButtonDisabled={isSubmitButtonDisabled}
      onSubmitBikeRent={onSubmitBikeRent}
      total={bikeRentAmount.value}
      bike={currentBikeData}
      setDateRange={setDateRange}
      dateRange={dateRange}
    />
  )
}

export default BikeDetailsContainer
