import { SERVICE_FEE_PERCENTAGE } from './BikeDetails.contants'
import { format } from 'date-fns'

export const getServicesFee = (amount: number): number =>
  Math.floor(amount * SERVICE_FEE_PERCENTAGE)

export const formatRangeDates = (dateRange: Date[]): string[] => {
  const dateFormat = 'yyyy-MM-dd'
  const dateFrom = format(dateRange[0] as Date, dateFormat)
  const dateTo = format(dateRange[1] as Date, dateFormat)
  return [dateFrom, dateTo]
}
