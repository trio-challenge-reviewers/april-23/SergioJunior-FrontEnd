import apiClient, { ApiResponseType } from './api'

type GetAmountType = {
  bikeId: number
  userId: number
  dateFrom: Date | string
  dateTo: Date | string
}

type GetAmountResponseType = {
  rentAmount: number
  fee: number
  totalAmount: number
}

export const getBikeRentAmount = async (
  params: GetAmountType,
): Promise<ApiResponseType<GetAmountResponseType>> => {
  return apiClient.post('/bikes/amount', params)
}

export const submitBikeRent = async (
  params: GetAmountType,
): Promise<ApiResponseType<GetAmountResponseType>> => {
  return apiClient.post('/bikes/rent', params)
}

export default { getBikeRentAmount, submitBikeRent }
